const pkg = require('./package');
const blogs = require('./content/blogs.json');

module.exports = {
  mode: 'universal',

  // Headers of the page
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },
    ],
  },

  // Customize the progress-bar color
  loading: { color: '#fff' },

  // Global CSS
  css: [
  ],

  // Plugins to load before mounting the App
  plugins: [
  ],

  //  Nuxt.js modules
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    // Doc: https://buefy.github.io/#/documentation
    ['nuxt-buefy', {
      materialDesignIcons: false,
    }],
    'nuxt-compress',
  ],
  // Axios module configuration
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  // generate config
  generate: {
    routes: [].concat(blogs.map((blog) => `/blog/${blog.slug}`)),
    dir: 'public',
  },

  // Build configuration
  build: {
    // You can extend webpack config here
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/,
        });
      }
      config.module.rules.push({
        test: /\.md$/,
        loader: 'frontmatter-markdown-loader',
        options: {
          vue: {
            root: 'dynamicContent',
          },
        },
      });
    },
  },
};
