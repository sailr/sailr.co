---
title: 'CI/CD: An Analogy'
date: 'Tuesday, Sep 18, 2019'
author: 'Jameson Stone'
authorlink: 'https://gitlab.com/jstone28'
photo: '/blogImages/ci-cd-analogy.jpg'
summary: 'Sometimes what you need is a good analogy to paint an old concept in a new light.'
---

*Photo by Tim Mossholder on Unsplash*

# CI/CD: An Analogy

**Continuous Integration** and **Continuous Delivery/Deployment** reign supreme among the most used cloud native buzzwords of the 2010's; and, often, there remains this sort of mysticism around the terms as if they're an insurmountable technical achievement and not, like most good things in computing, a practical idea. So, in place of writing more technical documentation, I'd like to make a more primitive argument. And to do that, I am going to use an analogy. My English professors all through high school and into University were always very clear about not using analogies when trying to explain something complex. Analogies rarely ever work and you often times lose half the audience; who now are mentally checked out trying to figure out how your analogy fits to the topic being discussed, and in how many ways you're actually wrong.

Nonetheless, analogies have a sort of power to bring a new perspective because, often times, problems are not unique to one field expertise, but are more common sense than we'd all first like to believe. So, today, we're going to take the topic of CI/CD development model versus more traditional models and attempt to cast it in a new light.

## The Analogy

![building a house](/blogImages/building-a-house.jpg)

It's often said that writing software is more like building a house than writing a paper or molding clay so let's start there.

Let's say you are the lead contractor building a home for yourself. You are in charge of hiring all the subcontractors to help tackle the project. You are also in charge of the final product, deadline, and the build quality.

Today you're installing electrical outlets. The interesting part about electrical outlets is that they each tie together so changing one may, necessarily, effect another but the work is fairly straight forward, and certainly something many people have done before so you proceed with the installation. The electrician can complete 1 outlet per day, there are 4 outlets required for installation. The cost breakdown looks like this:

* The electrician charges you $50 to "roll a truck" every time she must come out to your home; regardless of what changes are required
* Any charges thereafter are simply materials (largely negligible for this example)
* It costs you $1 of your own time to check the outlet for functionality

## Waterfall

![electrician](/blogImages/electrician.jpg)

Applying the waterfall methodology to this analogy, as the lead contractor I would:

* Call the electrician out to do the installation ($50)
* Incur some charge for the materials (negligible)
* When the electrician is done, we wait some predetermined time (say, 1 month) to check the correctness of the installation ($4)

In this simple model, minimum cost allowed is $54 but because electrical work is complex and has a tendency towards chaos, and because our house is in an old neighborhood, the maximal cost is infinite due to the fact that the electrician may continually make mistakes or may need to make adjustments, and we may continue to wait predetermined amount of time before checking the quality of work. Additionally, the timeline is bounded only by our patience to allow the electrician to continue to make mistakes or changes. That is because we've decided, from the outset, that we're only going to check the work at the end of every month -- consequently -- we won't know whether the electrician must make another trip until that time. That means the overall predictability of this model is extremely low. Equivalent to, say, guess and check.

## Agile-Waterfall

By far the most common of methodologies is the agile-waterfall methodology wherein:

* Call the electrician out to do the installation $50
* Incurr some charge for the materials (negligible)
* When the electrician is done, we wait some (lesser) predetermined time (say, 1 week) to check the correctness of the installation ($4)

In this model, the same lower bound exists $54 and the same maximum exists because we're still only checking after a predetermined period of time. The predictability of this model, however, is higher because our intervals are shorter. Consequently, we can know (more quickly) if we're going to miss our deadline because we're able to determine success on weekly intervals (as opposed to monthly).

## CI/CD-Agile

A more modern approach would be to employ a continuous integration, continuous delivery methodology wherein:

* Call the electrian out to do the installation $50
* Incurr some charge for the materials (neglible)
* When the electrician is done on a per-outlet basis, we check her work.

In this model, the same lower bound as the other models exist however, because we're checking as the work is done we can verify the correctness as the work is complete. That means our upper bound is capped. The electrician cannot charge to roll another truck because we won't need to, we're verifying the work as it's done. Moreover, we're insured to only ever having to check each outlet once (as it's done) and by that process we can know if a new installation broke a previous installation. Finally, the completion date can be calculated almost programmatically as installation and testing can be described on a per outlet basis, and therefore are almost known variables.

The other thing to point out in this analogy is the emergence of "choke points". The choke point in the waterfall model is a predetermined period of work (1 month). In the agile-waterfall model, the choke point is another predetermined period although it's shorter (1 week). Finally, out of the CI/CD-Agile methodology, emerges a natural choke point: when each outlet is completed, sequentially; making it a more natural transition for the process as a whole.

Hopefully that adds some clarification to the idea of continuous integration, continuous delivery/deployment, and helps to demystify it as an insurmountable technical problem, and expose it to be precisely as you thought, a practical idea. The tooling around CI/CD will emerge naturally as you team begins to consider what can be optimize, and what can be automated.
