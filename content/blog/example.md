---
title: 'Creating a Blogging platform with Nuxt'
date: 'Tuesday, Jun 4, 2019'
author: 'Madison Grubb'
authorlink: 'https://github.com/kgrubb'
photo: '/blogImages/example.jpg'
summary: 'This is just an example blog, used as a placeholder until we have more blog content available.'
---

Blog posts are written using [Markdown](https://en.wikipedia.org/wiki/Markdown).

It's very easy to make some words **bold** and other words _italic_ with Markdown. You can even [link to Google!](http://google.com)

# This is an h1 tag

## This is an h2 tag

###### This is an h6 tag

## Emphasis

_This text will be italic_
_This will also be italic_

**This text will be bold**
**This will also be bold**

_You **can** combine them_

## Lists

- Item 1
- Item 2
  - Item 2a
  - Item 2b

1. Item 1
1. Item 2
1. Item 3
   1. Item 3a
   1. Item 3b

## Blockquotes

As Samwise Gamgee said:

> PO-TAY-TOES

## Code

```js
const thisisatest = 'teststring';
// this is an example
const exampleFunction = () => {
  console.log('example');
};
```

```
// no syntax highlighting specified in backticks
const thisisatest = 'teststring';
// this is an example
const exampleFunction = () => {
  console.log('example');
};
```

[This is a link to somewhere](www.google.com)

this is an image:
![alt text](https://github.com/adam-p/markdown-here/raw/master/src/common/images/icon48.png "Logo Title Text 1")

This is a local image:

![alt stuff](/blogImages/kuber.jpg)
